<?php

namespace App\DataFixtures;

use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\BookableDate;
use App\Entity\User;
use App\Entity\Admin;
use DateTime;
use DateInterval;

class AppFixtures extends Fixture
{
    private UserPasswordHasherInterface $hasher;

    public function __construct(UserPasswordHasherInterface $hasher)
    {
        $this->hasher = $hasher;
    }

    public function load(ObjectManager $manager): void
    {
        for($i = 0; $i < 365; $i++){
            $bookableDate = new BookableDate();
            $bDate = new DateTime('NOW');

            if($i > 0){
                $bDate->add(new DateInterval('P'.$i.'D'));
            }
            $bookableDate->setDate( new DateTime($bDate->format('Y-m-d')) );

            $randomInt = rand(3, 10);
            if( $i>0 && $i % $randomInt == 0){
                $bookableDate->setIsActive(0);
                $bookableDate->setTotalVacancy(0);
                $bookableDate->setVacancyPrice(0);
                $bookableDate->setTotalBooked(0);
                $bookableDate->setCreatedAt(new DateTime('NOW'));
            }else{
                $bookableDate->setIsActive(1);
                $bookableDate->setTotalVacancy( (float)rand(5, 15) );
                $bookableDate->setVacancyPrice( (float)$randomInt );
                $bookableDate->setTotalBooked( rand(0,2) );
                $bookableDate->setCreatedAt(new DateTime('NOW'));
            }
            $manager->persist($bookableDate);

            $manager->flush();
        }

        //user fixtures
        $user = new User();
        $password = $this->hasher->hashPassword($user, '111111');
        $user->setPassword($password);
        $user->setName('Test User');
        $user->setEmail('test@gmail.com');
        $user->setIsVerified(1);
        $user->setStatus(1);
        $user->setCreatedAt(new DateTime('NOW'));
        $user->setRoles(['ROLE_USER']);

        $manager->persist($user);
        $manager->flush();

        //Admin fixtures
        $admin = new Admin();
        $password = $this->hasher->hashPassword($user, '222222');
        $admin->setPassword($password);
        $admin->setName('Test Admin');
        $admin->setEmail('admin@gmail.com');
        $admin->setRoles(['ROLE_ADMIN']);

        $manager->persist($admin);
        $manager->flush();
    }
}
