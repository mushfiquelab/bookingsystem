<?php

namespace App\Entity;

use App\Repository\ReservationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ReservationRepository::class)]
class Reservation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $title;

    #[ORM\Column(type: 'text', nullable: true)]
    private $descr;

    #[ORM\Column(type: 'date')]
    private $start_date;

    #[ORM\Column(type: 'date')]
    private $end_date;

    #[ORM\Column(type: 'float')]
    private $total_cost;

    #[ORM\Column(type: 'integer')]
    private $total_vacancy_booked;

    #[ORM\Column(type: 'integer')]
    private $status;

    #[ORM\Column(type: 'datetime')]
    private $created_at;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private $updated_at;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'reservations')]
    #[ORM\JoinColumn(nullable: false)]
    private $user;

    #[ORM\ManyToMany(targetEntity: BookableDate::class, mappedBy: 'reservations')]
    private $bookableDates;

    public function __construct()
    {
        $this->bookableDates = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescr(): ?string
    {
        return $this->descr;
    }

    public function setDescr(?string $descr): self
    {
        $this->descr = $descr;

        return $this;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->start_date;
    }

    public function setStartDate(\DateTimeInterface $start_date): self
    {
        $this->start_date = $start_date;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->end_date;
    }

    public function setEndDate(\DateTimeInterface $end_date): self
    {
        $this->end_date = $end_date;

        return $this;
    }

    public function getTotalCost(): ?float
    {
        return $this->total_cost;
    }

    public function setTotalCost(float $total_cost): self
    {
        $this->total_cost = $total_cost;

        return $this;
    }

    public function getTotalVacancyBooked(): ?int
    {
        return $this->total_vacancy_booked;
    }

    public function setTotalVacancyBooked(int $total_vacancy_booked): self
    {
        $this->total_vacancy_booked = $total_vacancy_booked;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|BookableDate[]
     */
    public function getBookableDates(): Collection
    {
        return $this->bookableDates;
    }

    public function addBookableDate(BookableDate $bookableDate): self
    {
        if (!$this->bookableDates->contains($bookableDate)) {
            $this->bookableDates[] = $bookableDate;
            $bookableDate->addReservation($this);
        }

        return $this;
    }

    public function removeBookableDate(BookableDate $bookableDate): self
    {
        if ($this->bookableDates->removeElement($bookableDate)) {
            $bookableDate->removeReservation($this);
        }

        return $this;
    }
}
