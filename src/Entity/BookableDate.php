<?php

namespace App\Entity;

use App\Repository\BookableDateRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: BookableDateRepository::class)]
class BookableDate
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'date')]
    private $date;

    #[ORM\Column(type: 'integer')]
    private $total_vacancy;

    #[ORM\Column(type: 'integer')]
    private $total_booked;

    #[ORM\Column(type: 'float')]
    private $vacancy_price;

    #[ORM\Column(type: 'boolean')]
    private $is_active;

    #[ORM\Column(type: 'datetime')]
    private $created_at;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private $updated_at;

    #[ORM\ManyToMany(targetEntity: Reservation::class, inversedBy: 'bookableDates')]
    private $reservations;

    public function __construct()
    {
        $this->reservations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getTotalVacancy(): ?int
    {
        return $this->total_vacancy;
    }

    public function setTotalVacancy(int $total_vacancy): self
    {
        $this->total_vacancy = $total_vacancy;

        return $this;
    }

    public function getTotalBooked(): ?int
    {
        return $this->total_booked;
    }

    public function setTotalBooked(int $total_booked): self
    {
        $this->total_booked = $total_booked;

        return $this;
    }

    public function getVacancyPrice(): ?float
    {
        return $this->vacancy_price;
    }

    public function setVacancyPrice(float $vacancy_price): self
    {
        $this->vacancy_price = $vacancy_price;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->is_active;
    }

    public function setIsActive(bool $is_active): self
    {
        $this->is_active = $is_active;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return Collection|Reservation[]
     */
    public function getReservations(): Collection
    {
        return $this->reservations;
    }

    public function addReservation(Reservation $reservation): self
    {
        if (!$this->reservations->contains($reservation)) {
            $this->reservations[] = $reservation;
        }

        return $this;
    }

    public function removeReservation(Reservation $reservation): self
    {
        $this->reservations->removeElement($reservation);

        return $this;
    }
}
