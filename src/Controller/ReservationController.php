<?php
declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\BookableDate;
use App\Entity\Reservation;
use App\Entity\User;
use DateTime;

final class ReservationController extends AbstractController
{
    private $doctrine;

    public function __construct(ManagerRegistry $doctrine){
        $this->doctrine = $doctrine;
    }

    #[Route('/api/my-reservations', name: 'my_reservations')]
    public function myReservations(): Response
    {
        if($this->getUser()){
            $objReservations = $this->doctrine->getRepository(Reservation::class)->findByUser($this->getUser());
            if(count($objReservations) > 0){
                $objReservations = $this->_formatMyReservations($objReservations);
                return $this->json([
                    'result' => 'success',
                    'reservations' => $objReservations
                ]);
            }else{
                return $this->json([
                    'result' => 'error',
                    'message' => 'You have no reservation booked! Please book first'
                ]);
            }
        }
        return $this->json([
            'result' => 'error',
            'message' => 'You are not logged in! Please login first.'
        ]);
    }

    #[Route('/api/new-reservation', name: 'new_reservation')]
    public function newReservation(Request $request): Response
    {
        $data = json_decode($request->getContent());

        $this->_getDateFromString($data->start_date);

        $entityManager = $this->doctrine->getManager();
        $reservation = new Reservation();
        $reservation->setTitle($data->start_date .' - '. $data->end_date);
        $reservation->setStartDate($this->_getDateFromString($data->start_date));
        
        $endDate = $this->_getDateFromString($data->end_date);
        if($endDate){
            $reservation->setEndDate($endDate);
        }else{
            $reservation->setEndDate($this->_getDateFromString($data->start_date));
        }
        $reservation->setTotalCost($data->total_cost);
        $reservation->setTotalVacancyBooked($data->total_vacancy_booked);
        $reservation->setStatus(1);
        $reservation->setUser($this->getUser());
        $reservation->setCreatedAt(new DateTime('NOW'));
        $entityManager->persist($reservation);
        $entityManager->flush();

        //now need to update bookable_date table
        $this->doctrine->getRepository(BookableDate::class)->updateTotalBookedCount($reservation->getStartDate(), $reservation->getEndDate());

        return $this->json([
            'result' => 'success',
            'message' => 'Reservation has been saved! Thanks.'
        ]);
    }

    protected function _getDateFromString($dateString){
        if(strpos($dateString, '/')!==false){
            $dateString = str_replace('/', '-',$dateString);
            $timeVal = strtotime($dateString);
            $date = date('Y-m-d', $timeVal);
            if($date){
                return new DateTime($date);
            }
        }else if(strpos($dateString, '-')!==false){
            $timeVal = strtotime($dateString);
            $date = date('Y-m-d', $timeVal);
            if($date){
                return new DateTime($date);
            }
        }
        return false;
    }

    protected function _formatMyReservations($objReservations){
        foreach($objReservations as $i => $obj){
            $objReservations[$i]['start_date'] = $objReservations[$i]['start_date']->format('d-M-Y');
            $objReservations[$i]['end_date'] = $objReservations[$i]['end_date']->format('d-M-Y');
            $objReservations[$i]['created_at'] = $objReservations[$i]['created_at']->format('Y-m-d H:i:s');
            if($objReservations[$i]['updated_at']){
                $objReservations[$i]['updated_at'] = $objReservations[$i]['updated_at']->format('Y-m-d H:i:s');
            }
        }
        return $objReservations;
    }
}
