<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\User;
use DateTime;

class SignUpController extends AbstractController
{
    private $doctrine;
    private $passwordHasher;

    public function __construct(ManagerRegistry $doctrine, UserPasswordHasherInterface $passwordHasher){
        $this->doctrine = $doctrine;
        $this->passwordHasher = $passwordHasher;
    }

    #[Route('/api/sign-up', name: 'sign_up')]
    public function signup(Request $request): Response
    {
        $data = json_decode($request->getContent());
        $entityManager = $this->doctrine->getManager();
        $user = new User();
        $user->setName($data->name);
        $user->setEmail($data->email);
        
        $hashedPassword = $this->passwordHasher->hashPassword(
            $user,
            $data->password
        );
        $user->setPassword($hashedPassword);
        $user->setRoles(['ROLE_USER']);
        $user->setIsVerified(0);
        // $createdAt = date('d-m-Y H:i:s', time());
        $user->setCreatedAt(new DateTime('NOW'));
        $entityManager->persist($user);
        $entityManager->flush();

        return $this->json([
            'message' => 'User Registration successful!'
        ]);
    }
}
