<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use App\Entity\User;

class ApiLoginController extends AbstractController
{
    #[Route('/api/login', name: 'api_login')]
    public function login(#[CurrentUser] ?User $user): Response
    {
        // if(null === $user ) {
        //     return $this->json(['message' => 'missing credentials'], Response::HTTP_UNAUTHORIZED);
        // }

        $loggedInUser = [];
        if( $this->getUser() ){
            if($this->getUser()->getIsVerified()){
                $loggedInUser['id'] = $this->getUser()->getId();
                $loggedInUser['email'] = $this->getUser()->getEmail();
                $token = password_hash($loggedInUser['id'].$loggedInUser['email'], PASSWORD_DEFAULT);
                $loggedInUser['token'] = $token;
                $loggedInUser['name'] = $this->getUser()->getName();
            }else{
                return $this->json([
                    'result' => 'error',
                    'message' => 'Account is not verified yet! Please check email for verification.',
                ]);
            }
        }
        if($loggedInUser){
            return $this->json([
                'result' => 'success',
                'user' => $loggedInUser
            ]);
        }
        return $this->json([
            'result' => 'error',
            'message' => 'Login failed! Please try again.',
        ]);
    }

    /**
     * @Route("/api/logout", name="api_logout", methods={"GET"})
     */
    public function logout(): void
    {
    }
}
