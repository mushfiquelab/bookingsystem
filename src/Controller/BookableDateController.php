<?php
declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\BookableDate;
use App\Entity\User;
use DateTime;

final class BookableDateController extends AbstractController
{
    private $doctrine;

    public function __construct(ManagerRegistry $doctrine){
        $this->doctrine = $doctrine;
    }

    #[Route('/api/get-bookables', name: 'get_bookables')]
    public function getBookables(): Response
    {
        if($this->getUser()){
            $objBookables = $this->doctrine->getRepository(BookableDate::class)->filteredBookables('');
            if(count($objBookables) > 0){
                $objBookables = $this->_formatMyBookables($objBookables);
                return $this->json([
                    'result' => 'success',
                    'bookable_dates' => $objBookables
                ]);
            }else{
                return $this->json([
                    'result' => 'error',
                    'message' => 'You have no BookableDate booked! Please book first'
                ]);
            }
        }
        return $this->json([
            'result' => 'error',
            'message' => 'You are not logged in! Please login first.'
        ]);
    }

    #[Route('/api/get-disabled-dates', name: 'get_disabled_dates')]
    public function getDisableDates(Request $request): Response
    {
        if($this->getUser()){
            $data = json_decode($request->getContent());
            $params = [];
            if($data){
                $params['year'] = date('Y');
                $params['month'] = $data->month;
            }
            $disabledDates = $this->doctrine->getRepository(BookableDate::class)->getDisabledDates($params);
            if(count($disabledDates) > 0){
                return $this->json([
                    'result' => 'success',
                    'disabledDates' => $disabledDates
                ]);
            }else{
                return $this->json([
                    'result' => 'success',
                    'disabledDates' => []
                ]);
            }
        }
        return $this->json([
            'result' => 'error',
            'message' => 'You are not logged in! Please login first.'
        ]);
    }

    #[Route('/api/get-filtered-bookables', name: 'get_filtered_bookables')]
    public function getFilteredBookables(Request $request): Response
    {
        if($this->getUser()){
            $data = json_decode($request->getContent());
            $params = [];
            if($data){
                $params['year'] = $data->year;
                $params['month'] = $data->month;
            }
            $objBookables = $this->doctrine->getRepository(BookableDate::class)->filteredBookables($params);
            if(count($objBookables) > 0){
                $objBookables = $this->_formatMyBookables($objBookables);
                return $this->json([
                    'result' => 'success',
                    'bookable_dates' => $objBookables
                ]);
            }else{
                return $this->json([
                    'result' => 'error',
                    'message' => 'Either no vacancy found or dates are not allowed to be booked.'
                ]);
            }
        }
        return $this->json([
            'result' => 'error',
            'message' => 'You are not logged in! Please login first.'
        ]);
    }

    protected function _formatMyBookables($objBookables){
        foreach($objBookables as $i => $obj){
            $objBookables[$i]['date'] = $objBookables[$i]['date']->format('d-M-Y');
            $objBookables[$i]['created_at'] = $objBookables[$i]['created_at']->format('Y-m-d H:i:s');
            if($objBookables[$i]['updated_at']){
                $objBookables[$i]['updated_at'] = $objBookables[$i]['updated_at']->format('Y-m-d H:i:s');
            }
        }
        return $objBookables;
    }

    #[Route('/api/get-cost-n-vacancies', name: 'get_cost_n_vacancies')]
    public function getCostNvacancies(Request $request): Response
    {
        if($this->getUser()){
            $data = json_decode($request->getContent());
            if($data){
                $params = [];
                if($data){
                    $params['start_date'] = $data->start_date;
                    $params['end_date'] = $data->end_date;

                    //formatting date pattern 
                    $params['start_date'] = str_replace('/', '-',$params['start_date']);
                    $timeVal = strtotime($params['start_date']);
                    $params['start_date'] = date('Y-m-d', $timeVal);

                    $params['end_date'] = str_replace('/', '-',$params['end_date']);
                    $timeVal = strtotime($params['end_date']);
                    $params['end_date'] = date('Y-m-d', $timeVal);
                }
                $costNvacancies = $this->doctrine->getRepository(BookableDate::class)->getCostNvacancies($params);
                if($costNvacancies['result'] == 'success'){
                    return $this->json($costNvacancies);
                }else{
                    return $this->json([
                        'result' => 'error',
                        'message' => 'Either no vacancy found or dates are not allowed to be booked.'
                    ]);
                }
            }else{
                return $this->json([
                    'result' => 'error',
                    'message' => 'You did not select any date to book! Please select first.'
                ]);
            }
        }
        return $this->json([
            'result' => 'error',
            'message' => 'You are not logged in! Please login first.'
        ]);
    }
}
