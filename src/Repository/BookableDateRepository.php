<?php

namespace App\Repository;

use App\Entity\BookableDate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BookableDate|null find($id, $lockMode = null, $lockVersion = null)
 * @method BookableDate|null findOneBy(array $criteria, array $orderBy = null)
 * @method BookableDate[]    findAll()
 * @method BookableDate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookableDateRepository extends ServiceEntityRepository
{
    private $doctrine;
    private $monthDates;

    public function __construct(ManagerRegistry $registry)
    {
        $this->doctrine = $registry;
        $this->monthDates = ['01' => 31, '02' => 28, '03' => 31, '04' => 30, '05' => 31, '06' => 30,
            '07' => 31, '08' => 31, '09' => 30, '10' => 31, '11' => 30, '12' => 31];
        parent::__construct($registry, BookableDate::class);
    }

    /**
     * @return BookableDate[] Returns an array of Reservation objects
     */
    public function filteredBookables($data = '')
    {
        if($data && isset($data['year']) && isset($data['month'])){
            $data['month'] = $data['month'] < 10 ? '0'.$data['month'] : $data['month'];
            $startingDate = $data['year'].'-'.$data['month'].'-'.'01';
            $endingDate = $data['year'].'-'.$data['month'].'-'.$this->monthDates[ $data['month'] ];

            return $this->createQueryBuilder('b')
                ->where('b.date <= :endingDate')
                ->setParameter('endingDate', $endingDate)
                ->andWhere('b.date >= :startingDate')
                ->setParameter('startingDate', $startingDate)
                ->orderBy('b.id', 'ASC')
                ->setMaxResults(31)
                ->getQuery()
                ->getArrayResult();
        }else{
            return $this->createQueryBuilder('b')
                ->orderBy('b.id', 'ASC')
                ->setMaxResults(31)
                ->getQuery()
                ->getArrayResult();
        }
    }

    public function getCostNvacancies($data)
    {
        $response['result'] = 'success';
        if($data && isset($data['start_date']) && isset($data['end_date'])){
            $activeBookables = $this->createQueryBuilder('b')
                ->where('b.date <= :endingDate')
                ->setParameter('endingDate', $data['end_date'])
                ->andWhere('b.date >= :startingDate')
                ->andWhere('b.is_active = 1')
                ->andWhere('b.total_vacancy > 0')
                ->setParameter('startingDate', $data['start_date'])
                ->orderBy('b.id', 'ASC')
                ->getQuery()
                ->getArrayResult();

            if(count($activeBookables) > 0){
                $totalCost = $totalBookables = 0;
                foreach($activeBookables as $acB){
                    if($acB['total_booked'] < $acB['total_vacancy']){
                        $totalBookables++;
                        $totalCost += $acB['vacancy_price'];
                    }
                }
                $response['total_cost'] = $totalCost;
                $response['total_bookable_vacancies'] = $totalBookables;
            }else{
                $response['result'] = 'error';
                $response['message'] = 'No bookable dates found with the selected dates';
            }
        }
        return $response;
    }

    public function updateTotalBookedCount($startDate, $endDate){
        $activeBookables = $this->createQueryBuilder('b')
                ->where('b.date <= :endDate')
                ->setParameter('endDate', $endDate)
                ->andWhere('b.date >= :startDate')
                ->andWhere('b.is_active = 1')
                ->andWhere('b.total_vacancy > 0')
                ->setParameter('startDate', $startDate)
                ->orderBy('b.id', 'ASC')
                ->getQuery()
                ->getResult();
        
        $entityManager = $this->doctrine->getManager();

        if(count($activeBookables) > 0){
            foreach($activeBookables as $acB){
                if($acB->getTotalBooked() < $acB->getTotalVacancy()){
                    $acB->setTotalBooked($acB->getTotalBooked() + 1);
                    $entityManager->persist($acB);
                    $entityManager->flush();
                }
            }
        }
    }

    public function getDisabledDates($data){
        $data['month'] = $data['month'] < 10 ? '0'.$data['month'] : $data['month'];
        $startDate = $data['year'].'-'.$data['month'].'-'.'01';
        $endDate = $data['year'].'-'.$data['month'].'-'.$this->monthDates[ $data['month'] ];

        $disabledBookables = $this->createQueryBuilder('b')
                ->where('b.date <= :endDate')
                ->setParameter('endDate', $endDate)
                ->andWhere('b.date >= :startDate')
                ->setParameter('startDate', $startDate)
                ->orderBy('b.id', 'ASC')
                ->getQuery()
                ->getArrayResult();
        
        $disabledDates = [];
        if(count($disabledBookables) > 0){
            foreach($disabledBookables as $dB){
                if($dB['total_vacancy'] == 0 || $dB['total_vacancy'] == $dB['total_booked']){
                    $disabledDates[] = $dB['date']->format('Y-m-d');
                }
            }
        }
        return $disabledDates;
    }

}
