# BookingSystem



## Installation

Please run the following command in terminal as a sudo user

```
git clone https://gitlab.com/mushfique.gitlab/bookingsystem.git
cd bookingsystem
docker-compose up
composer install
npm install
npm run dev
```

After the above steps please follow the following steps too
```
docker-compose exec php /bin/bash
php bin/console doctrine:migrations:migrate
php bin/console doctrine:fixtures:load
```

You will get the project in the following link
```
http://localhost:8080/login
```
For mac machine you may need to add the following line in the hosts file
```
127.0.0.1       localhost:8080
```

## Testing login credentials
```
test@gmail.com
111111
```

## Admin route and test credentials
```
http://localhost:8080/admin
admin@gmail.com
222222
```