import Vue from "vue";
import App from "./App";
import router from './router';

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        localStorage.getItem('BS.logged_in_user');
        if (!localStorage.getItem('BS.logged_in_user')) {
            next({ name: 'Login' });
        } else {
            next();
        }
    } else {
        next();
    }
});

new Vue({
    components: { App },
    template: "<App/>",
    router
}).$mount("#app");