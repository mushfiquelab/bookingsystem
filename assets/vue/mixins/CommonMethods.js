export default {
    methods: {
        show_toast: function (type, message) {
            if (type == 'success') {
                if ($('.toast').hasClass('toast-error')) {
                    $('.toast').removeClass('toast-error');
                }
                if ($('.toast').hasClass('toast-info')) {
                    $('.toast').removeClass('toast-info');
                }
                $('.toast').addClass('toast-success');
            } else if (type == 'error') {
                if ($('.toast').hasClass('toast-success')) {
                    $('.toast').removeClass('toast-success');
                }
                if ($('.toast').hasClass('toast-info')) {
                    $('.toast').removeClass('toast-info');
                }
                $('.toast').addClass('toast-error');
            } else if (type == 'info') {
                if ($('.toast').hasClass('toast-error')) {
                    $('.toast').removeClass('toast-error');
                }
                if ($('.toast').hasClass('toast-success')) {
                    $('.toast').removeClass('toast-success');
                }
                $('.toast').addClass('toast-info');
            }
            $('.toast-body').html(message);
            $('.toast').toast('show');
        },

        show_loader: function () {
            $('.bs-loader').css('visibility', 'visible');
        },

        hide_loader: function () {
            $('.bs-loader').css('visibility', 'hidden');
        },
    }
};
