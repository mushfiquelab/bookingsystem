import Vue from "vue";
import VueRouter from "vue-router";

import Login from "./pages/Login";
import Signup from "./pages/Signup";
import NewReservation from "./pages/NewReservation";
import MyReservations from "./pages/MyReservations";
import BookableDates from "./pages/BookableDates";
import Profile from "./pages/Profile";
import LandingPage from "./pages/LandingPage";

Vue.use(VueRouter);

export default new VueRouter({
    mode: "history",
    routes: [
        {
            path: "/",
            component: LandingPage,
            name: 'LandingPage'
        },
        {
            path: "/login",
            component: Login,
            name: "Login"
        },
        {
            path: "/sign-up",
            component: Signup,
            name: "Signup"
        },
        {
            path: "/new-reservation",
            component: NewReservation,
            name: "NewReservation",
            meta: {
                requiresAuth: true
            }
        },
        {
            path: "/my-reservations",
            component: MyReservations,
            name: "MyReservations",
            meta: {
                requiresAuth: true
            }
        },
        {
            path: "/profile",
            component: Profile,
            name: "Profile",
            meta: {
                requiresAuth: true
            }
        },
        {
            path: "/bookable-dates",
            component: BookableDates,
            name: "BookableDates",
            meta: {
                requiresAuth: true
            }
        },
    ]
});
