<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220115093144 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE bookable_date_reservation (bookable_date_id INT NOT NULL, reservation_id INT NOT NULL, INDEX IDX_76A2B0DFB8021C19 (bookable_date_id), INDEX IDX_76A2B0DFB83297E7 (reservation_id), PRIMARY KEY(bookable_date_id, reservation_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE bookable_date_reservation ADD CONSTRAINT FK_76A2B0DFB8021C19 FOREIGN KEY (bookable_date_id) REFERENCES bookable_date (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE bookable_date_reservation ADD CONSTRAINT FK_76A2B0DFB83297E7 FOREIGN KEY (reservation_id) REFERENCES reservation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE reservation ADD user_id INT NOT NULL');
        $this->addSql('ALTER TABLE reservation ADD CONSTRAINT FK_42C84955A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_42C84955A76ED395 ON reservation (user_id)');
        $this->addSql('ALTER TABLE user ADD name VARCHAR(50) NOT NULL, ADD is_verified TINYINT(1) NOT NULL, ADD pass_reset_token VARCHAR(255) DEFAULT NULL, ADD token_sent_at DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE bookable_date_reservation');
        $this->addSql('ALTER TABLE reservation DROP FOREIGN KEY FK_42C84955A76ED395');
        $this->addSql('DROP INDEX IDX_42C84955A76ED395 ON reservation');
        $this->addSql('ALTER TABLE reservation DROP user_id');
        $this->addSql('ALTER TABLE user DROP name, DROP is_verified, DROP pass_reset_token, DROP token_sent_at');
    }
}
